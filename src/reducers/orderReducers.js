import { BUY_CLICK } from "../constant/order.constant";


const initialState = {
    prices: 0,
    mobileList: [
        {
            name: "IPhone X",
            price: 900,
            quantity: 0,
            id:1
        },
        {
            name: "Samsung S9",
            price: 800,
            quantity: 0,
            id:2
        },
        {
            name: "Nokia 8",
            price: 650,
            quantity: 0,
            id:3
        }
    ]
}

const orderReducer = (state = initialState, action) => {
    switch (action.type) {
        case BUY_CLICK:
            const index = action.payload
            
            state.mobileList[index].quantity = (state.mobileList[index].quantity) + 1
            state.prices = state.prices + state.mobileList[index].price
            break;
        default:
            break;
    }
    return { ...state }
}

export default orderReducer 