import { BUY_CLICK } from "../constant/order.constant"

export const buyClick = (index) => {
    return {
        type: BUY_CLICK,
        payload: index 
    }
}