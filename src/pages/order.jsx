import { Typography } from "@mui/material"
import OrderContent from "../components/orderContent"

const Order = () => {

    return (
        <div className="container mt-4">
            <Typography variant="h6">
                Mobile App
            </Typography>
            <OrderContent />
            
        </div>
    )
}

export default Order