import { useSelector } from "react-redux"
import { Card, CardActions, CardContent, Button, Typography } from "@mui/material"
import { useNavigate, useParams } from "react-router-dom"

const Detail = () => {
    const { mobileList } = useSelector((reduxData) => {
        return reduxData.orderReducer
    })
    const {param} = useParams();
    
    const [item] = mobileList.filter((item) => {
        return item.id == param
    });
    let naviagte = useNavigate()
    const onBtnBack = () => {
        naviagte("/")
    }
    return (
        <div className="mt-4" style={{ display: "flex", justifyContent: "space-around" }}>
            <Card className="p-3" sx={{ width: "90%" }}>
                <CardContent>
                    <Typography sx={{ fontSize: 14, mb: 2 }} variant="body1" gutterBottom>
                        <b>{item.name}</b>
                    </Typography>

                    <Typography sx={{ mb: 2 }} variant="body2" color="text.secondary">
                        Price: {item.price} USD
                    </Typography>
                    <Typography variant="body2">
                        Quantity: {item.quantity}
                    </Typography>
                </CardContent>
                <CardActions>
                    <Button onClick={onBtnBack} variant="contained" color="success">Back</Button>
                </CardActions>
            </Card>
        </div>
    )
}

export default Detail