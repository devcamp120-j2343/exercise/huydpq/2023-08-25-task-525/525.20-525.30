import './App.css';
import "bootstrap/dist/css/bootstrap.min.css"
import Order from './pages/order';
import { Route, Routes } from 'react-router-dom';
import Detail from './components/detail';

function App() {
  return (
    <>
    <Routes>
      <Route path='/' element={<Order />}></Route>
      <Route path='/:param' element={<Detail />}></Route>
    </Routes>
    </>
  );
}

export default App;
